﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSync
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
        }
        DAL.Tables.TSyncProject project
        {
            get
            {
                return tSyncProjectBindingSource.Current as DAL.Tables.TSyncProject;
            }
        }
        private void 服务服务端ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MDL.CPublic.syncManager.Stop();
            using (Form_Client form = new Form_Client())
                form.ShowDialog();
            MDL.CPublic.syncManager.Start();
            tClientBindingSource.DataSource = MDL.CPublic.dc.TClient.ToTable();
           
        }

        private void Form_Main_Load(object sender, EventArgs e)
        {
            tClientBindingSource.DataSource = MDL.CPublic.dc.TClient.ToTable();
            tSyncProjectBindingSource.DataSource = MDL.CPublic.dc.TSyncProject.ToTable();
            MDL.CPublic.syncManager.onMsg = (msg, count, length) => {
                ClassLibrary_Public.WinForm.Invoke(() => {
                    toolStripStatusLabel_Msg.Text = msg;
                    toolStripProgressBar1.Maximum = length;
                    toolStripProgressBar1.Value = count;
                    toolStripStatusLabel_Par.Text = length == 0 ? "" : string.Format("{0:N2}K/{1:N2}K", (decimal)count / 1024, (decimal)length / 1024);
                });
            };
        }

        private void 新增ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MDL.CPublic.syncManager.Stop();
            using (Form_Project form = new Form_Project())
                form.ShowDialog();
            MDL.CPublic.syncManager.Start();
            tSyncProjectBindingSource.DataSource = MDL.CPublic.dc.TSyncProject.ToTable();
        }

        private void 修改ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pro = project;
            if (pro == null) return;
            MDL.CPublic.syncManager.Stop();

            using (Form_Project form = new Form_Project() { project = project })
                form.ShowDialog();
            MDL.CPublic.syncManager.Start();
            tSyncProjectBindingSource.DataSource = MDL.CPublic.dc.TSyncProject.ToTable().ToArray();
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pro = project;
            if (pro == null) return;
            MDL.CPublic.syncManager.Stop();
            MDL.CPublic.syncManager.Start();
            tSyncProjectBindingSource.DataSource = MDL.CPublic.dc.TSyncProject.ToTable().ToArray();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tSyncProjectBindingSource.ResetBindings(false);
        }
    }
}
