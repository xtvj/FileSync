﻿namespace FileSync
{
    partial class Form_Project
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox_CanWrite = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_ProjectName = new System.Windows.Forms.TextBox();
            this.comboBox_Client = new System.Windows.Forms.ComboBox();
            this.tClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBox_Path = new System.Windows.Forms.TextBox();
            this.textBox_ReadKey = new System.Windows.Forms.TextBox();
            this.textBox_WriteKey = new System.Windows.Forms.TextBox();
            this.button_Save = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_Path = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBox_IsSource = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.tClientBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "项目名称：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "连接端：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "同步目录：";
            // 
            // checkBox_CanWrite
            // 
            this.checkBox_CanWrite.AutoSize = true;
            this.checkBox_CanWrite.Location = new System.Drawing.Point(366, 139);
            this.checkBox_CanWrite.Name = "checkBox_CanWrite";
            this.checkBox_CanWrite.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox_CanWrite.Size = new System.Drawing.Size(18, 17);
            this.checkBox_CanWrite.TabIndex = 3;
            this.checkBox_CanWrite.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(308, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "读写：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "读取Key：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "写入Key：";
            // 
            // textBox_ProjectName
            // 
            this.textBox_ProjectName.Location = new System.Drawing.Point(137, 10);
            this.textBox_ProjectName.Name = "textBox_ProjectName";
            this.textBox_ProjectName.Size = new System.Drawing.Size(245, 25);
            this.textBox_ProjectName.TabIndex = 7;
            // 
            // comboBox_Client
            // 
            this.comboBox_Client.DataSource = this.tClientBindingSource;
            this.comboBox_Client.DisplayMember = "ClientName";
            this.comboBox_Client.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Client.FormattingEnabled = true;
            this.comboBox_Client.Location = new System.Drawing.Point(137, 52);
            this.comboBox_Client.Name = "comboBox_Client";
            this.comboBox_Client.Size = new System.Drawing.Size(245, 23);
            this.comboBox_Client.TabIndex = 8;
            this.comboBox_Client.ValueMember = "ClientID";
            // 
            // tClientBindingSource
            // 
            this.tClientBindingSource.DataSource = typeof(FileSync.DAL.Tables.TClient);
            // 
            // textBox_Path
            // 
            this.textBox_Path.Location = new System.Drawing.Point(137, 90);
            this.textBox_Path.Name = "textBox_Path";
            this.textBox_Path.Size = new System.Drawing.Size(245, 25);
            this.textBox_Path.TabIndex = 9;
            // 
            // textBox_ReadKey
            // 
            this.textBox_ReadKey.Location = new System.Drawing.Point(137, 170);
            this.textBox_ReadKey.Name = "textBox_ReadKey";
            this.textBox_ReadKey.Size = new System.Drawing.Size(245, 25);
            this.textBox_ReadKey.TabIndex = 10;
            // 
            // textBox_WriteKey
            // 
            this.textBox_WriteKey.Location = new System.Drawing.Point(137, 210);
            this.textBox_WriteKey.Name = "textBox_WriteKey";
            this.textBox_WriteKey.Size = new System.Drawing.Size(245, 25);
            this.textBox_WriteKey.TabIndex = 11;
            // 
            // button_Save
            // 
            this.button_Save.Location = new System.Drawing.Point(113, 265);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(75, 38);
            this.button_Save.TabIndex = 12;
            this.button_Save.Text = "保存";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(285, 265);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 38);
            this.button_Cancel.TabIndex = 13;
            this.button_Cancel.Text = "取消";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_Path
            // 
            this.button_Path.Location = new System.Drawing.Point(388, 92);
            this.button_Path.Name = "button_Path";
            this.button_Path.Size = new System.Drawing.Size(56, 23);
            this.button_Path.TabIndex = 14;
            this.button_Path.Text = "选择";
            this.button_Path.UseVisualStyleBackColor = true;
            this.button_Path.Click += new System.EventHandler(this.button_Path_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(49, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 15);
            this.label7.TabIndex = 16;
            this.label7.Text = "源文件端：";
            // 
            // checkBox_IsSource
            // 
            this.checkBox_IsSource.AutoSize = true;
            this.checkBox_IsSource.Location = new System.Drawing.Point(137, 137);
            this.checkBox_IsSource.Name = "checkBox_IsSource";
            this.checkBox_IsSource.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox_IsSource.Size = new System.Drawing.Size(18, 17);
            this.checkBox_IsSource.TabIndex = 15;
            this.checkBox_IsSource.UseVisualStyleBackColor = true;
            this.checkBox_IsSource.CheckedChanged += new System.EventHandler(this.checkBox_IsSource_CheckedChanged);
            // 
            // Form_Project
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 335);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.checkBox_IsSource);
            this.Controls.Add(this.button_Path);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.textBox_WriteKey);
            this.Controls.Add(this.textBox_ReadKey);
            this.Controls.Add(this.textBox_Path);
            this.Controls.Add(this.comboBox_Client);
            this.Controls.Add(this.textBox_ProjectName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkBox_CanWrite);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Project";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "同步项目设置";
            this.Load += new System.EventHandler(this.Form_Project_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tClientBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox_CanWrite;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_ProjectName;
        private System.Windows.Forms.ComboBox comboBox_Client;
        private System.Windows.Forms.TextBox textBox_Path;
        private System.Windows.Forms.TextBox textBox_ReadKey;
        private System.Windows.Forms.TextBox textBox_WriteKey;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.BindingSource tClientBindingSource;
        private System.Windows.Forms.Button button_Path;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox_IsSource;
    }
}