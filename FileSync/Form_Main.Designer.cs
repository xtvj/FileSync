﻿namespace FileSync
{
    partial class Form_Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel_Msg = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel_Par = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tSyncProjectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.服务服务端ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.同步项目ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.新增ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.修改ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.projectIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rootPathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountDir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsSerer = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.canWriteDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.readKeyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.writeKeyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifyTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSyncProjectBindingSource)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tClientBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.dataGridView1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(874, 427);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(874, 480);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.menuStrip1);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel_Msg,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel_Par});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(874, 25);
            this.statusStrip1.TabIndex = 0;
            // 
            // toolStripStatusLabel_Msg
            // 
            this.toolStripStatusLabel_Msg.Name = "toolStripStatusLabel_Msg";
            this.toolStripStatusLabel_Msg.Size = new System.Drawing.Size(41, 20);
            this.toolStripStatusLabel_Msg.Text = "Msg";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 19);
            // 
            // toolStripStatusLabel_Par
            // 
            this.toolStripStatusLabel_Par.Name = "toolStripStatusLabel_Par";
            this.toolStripStatusLabel_Par.Size = new System.Drawing.Size(33, 20);
            this.toolStripStatusLabel_Par.Text = "0/0";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.projectIDDataGridViewTextBoxColumn,
            this.projectNameDataGridViewTextBoxColumn,
            this.clientIDDataGridViewTextBoxColumn,
            this.rootPathDataGridViewTextBoxColumn,
            this.CountDir,
            this.CountFile,
            this.IsSerer,
            this.canWriteDataGridViewCheckBoxColumn,
            this.readKeyDataGridViewTextBoxColumn,
            this.writeKeyDataGridViewTextBoxColumn,
            this.modifyTimeDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.tSyncProjectBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 27;
            this.dataGridView1.Size = new System.Drawing.Size(874, 427);
            this.dataGridView1.TabIndex = 0;
            // 
            // tSyncProjectBindingSource
            // 
            this.tSyncProjectBindingSource.DataSource = typeof(FileSync.DAL.Tables.TSyncProject);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.服务服务端ToolStripMenuItem,
            this.同步项目ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(874, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 服务服务端ToolStripMenuItem
            // 
            this.服务服务端ToolStripMenuItem.Name = "服务服务端ToolStripMenuItem";
            this.服务服务端ToolStripMenuItem.Size = new System.Drawing.Size(96, 24);
            this.服务服务端ToolStripMenuItem.Text = "服务服务端";
            this.服务服务端ToolStripMenuItem.Click += new System.EventHandler(this.服务服务端ToolStripMenuItem_Click);
            // 
            // 同步项目ToolStripMenuItem
            // 
            this.同步项目ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.新增ToolStripMenuItem,
            this.修改ToolStripMenuItem,
            this.删除ToolStripMenuItem});
            this.同步项目ToolStripMenuItem.Name = "同步项目ToolStripMenuItem";
            this.同步项目ToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.同步项目ToolStripMenuItem.Text = "同步项目";
            // 
            // 新增ToolStripMenuItem
            // 
            this.新增ToolStripMenuItem.Name = "新增ToolStripMenuItem";
            this.新增ToolStripMenuItem.Size = new System.Drawing.Size(114, 26);
            this.新增ToolStripMenuItem.Text = "新增";
            this.新增ToolStripMenuItem.Click += new System.EventHandler(this.新增ToolStripMenuItem_Click);
            // 
            // 修改ToolStripMenuItem
            // 
            this.修改ToolStripMenuItem.Name = "修改ToolStripMenuItem";
            this.修改ToolStripMenuItem.Size = new System.Drawing.Size(114, 26);
            this.修改ToolStripMenuItem.Text = "修改";
            this.修改ToolStripMenuItem.Click += new System.EventHandler(this.修改ToolStripMenuItem_Click);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(114, 26);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // tClientBindingSource
            // 
            this.tClientBindingSource.DataSource = typeof(FileSync.DAL.Tables.TClient);
            // 
            // projectIDDataGridViewTextBoxColumn
            // 
            this.projectIDDataGridViewTextBoxColumn.DataPropertyName = "ProjectID";
            this.projectIDDataGridViewTextBoxColumn.HeaderText = "ProjectID";
            this.projectIDDataGridViewTextBoxColumn.Name = "projectIDDataGridViewTextBoxColumn";
            this.projectIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.projectIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // projectNameDataGridViewTextBoxColumn
            // 
            this.projectNameDataGridViewTextBoxColumn.DataPropertyName = "ProjectName";
            this.projectNameDataGridViewTextBoxColumn.HeaderText = "项目名";
            this.projectNameDataGridViewTextBoxColumn.Name = "projectNameDataGridViewTextBoxColumn";
            this.projectNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // clientIDDataGridViewTextBoxColumn
            // 
            this.clientIDDataGridViewTextBoxColumn.DataPropertyName = "ClientID";
            this.clientIDDataGridViewTextBoxColumn.HeaderText = "连接端";
            this.clientIDDataGridViewTextBoxColumn.Name = "clientIDDataGridViewTextBoxColumn";
            this.clientIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.clientIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // rootPathDataGridViewTextBoxColumn
            // 
            this.rootPathDataGridViewTextBoxColumn.DataPropertyName = "RootPath";
            this.rootPathDataGridViewTextBoxColumn.HeaderText = "目录";
            this.rootPathDataGridViewTextBoxColumn.Name = "rootPathDataGridViewTextBoxColumn";
            this.rootPathDataGridViewTextBoxColumn.ReadOnly = true;
            this.rootPathDataGridViewTextBoxColumn.Width = 300;
            // 
            // CountDir
            // 
            this.CountDir.DataPropertyName = "CountDir";
            this.CountDir.HeaderText = "目录总数";
            this.CountDir.Name = "CountDir";
            this.CountDir.ReadOnly = true;
            // 
            // CountFile
            // 
            this.CountFile.DataPropertyName = "CountFile";
            this.CountFile.HeaderText = "文件总数";
            this.CountFile.Name = "CountFile";
            this.CountFile.ReadOnly = true;
            // 
            // IsSerer
            // 
            this.IsSerer.DataPropertyName = "IsServer";
            this.IsSerer.HeaderText = "是否文件源";
            this.IsSerer.Name = "IsSerer";
            this.IsSerer.ReadOnly = true;
            // 
            // canWriteDataGridViewCheckBoxColumn
            // 
            this.canWriteDataGridViewCheckBoxColumn.DataPropertyName = "CanWrite";
            this.canWriteDataGridViewCheckBoxColumn.HeaderText = "允许写入";
            this.canWriteDataGridViewCheckBoxColumn.Name = "canWriteDataGridViewCheckBoxColumn";
            this.canWriteDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // readKeyDataGridViewTextBoxColumn
            // 
            this.readKeyDataGridViewTextBoxColumn.DataPropertyName = "ReadKey";
            this.readKeyDataGridViewTextBoxColumn.HeaderText = "ReadKey";
            this.readKeyDataGridViewTextBoxColumn.Name = "readKeyDataGridViewTextBoxColumn";
            this.readKeyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // writeKeyDataGridViewTextBoxColumn
            // 
            this.writeKeyDataGridViewTextBoxColumn.DataPropertyName = "WriteKey";
            this.writeKeyDataGridViewTextBoxColumn.HeaderText = "WriteKey";
            this.writeKeyDataGridViewTextBoxColumn.Name = "writeKeyDataGridViewTextBoxColumn";
            this.writeKeyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modifyTimeDataGridViewTextBoxColumn
            // 
            this.modifyTimeDataGridViewTextBoxColumn.DataPropertyName = "ModifyTime";
            this.modifyTimeDataGridViewTextBoxColumn.HeaderText = "ModifyTime";
            this.modifyTimeDataGridViewTextBoxColumn.Name = "modifyTimeDataGridViewTextBoxColumn";
            this.modifyTimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.modifyTimeDataGridViewTextBoxColumn.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 480);
            this.Controls.Add(this.toolStripContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "文件同步";
            this.Load += new System.EventHandler(this.Form_Main_Load);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSyncProjectBindingSource)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tClientBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 服务服务端ToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource tSyncProjectBindingSource;
        private System.Windows.Forms.BindingSource tClientBindingSource;
        private System.Windows.Forms.ToolStripMenuItem 同步项目ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 新增ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 修改ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Msg;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Par;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rootPathDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountDir;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountFile;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsSerer;
        private System.Windows.Forms.DataGridViewCheckBoxColumn canWriteDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn readKeyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn writeKeyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modifyTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Timer timer1;
    }
}

