﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSync.MDL
{
    enum SyncStreamType
    {
        FileList,
        Stream,
    }
    /// <summary>
    /// 数据同步传输的包
    /// </summary>
    class SyncStreamPackage
    {
        public SyncStreamType type { get; set; }
        public string readerKey { get; set; }
        public string writerKey { get; set; }
        public string msg { get; set; }
        public DAL.SyncFile[] files { get; set; }
        /// <summary>
        /// 文件或目录属性
        /// </summary>
        System.IO.FileAttributes fileAttributes { get; set; }
        /// <summary>
        /// 文件或目录路径
        /// </summary>
        public string filePath { get; set; }
        /// <summary>
        /// 文件传输的数据流写入点
        /// </summary>
        public int fileStreamPostion { get; set; }
        /// <summary>
        /// 文件传输的数据
        /// </summary>
        public byte[] fileStreamData { get; set; }
        /// <summary>
        /// 文件总长度
        /// </summary>
        public int fileStreamLength { get; set; }
        /// <summary>
        /// 文件传输是否结束
        /// </summary>
        public bool fileStreamEnd { get; set; }
        /// <summary>
        /// 文件创建时间
        /// </summary>
        public DateTime? CreationTime { get; set; }
        /// <summary>
        /// 文件最后访问时间
        /// </summary>
        public DateTime? LastAccessTime { get; set; }
        /// <summary>
        /// 文件最后修改时间
        /// </summary>
        public DateTime? LastWriteTime { get; set; }
        /// <summary>
        /// 服务器端是否删除此文件
        /// </summary>
        public bool isDeleted { get; set; }
    }
}
