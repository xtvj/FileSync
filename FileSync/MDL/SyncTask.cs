﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
namespace FileSync.MDL
{
    internal class SyncTask
    {
        internal enum Statu
        {
            waiting=0,
            downLoad=1,
            downLoaded=2
        }
        internal Socket socket { get; set; }
        internal SyncStreamPackage pak { get; set; }
        /// <summary>
        /// 
        /// </summary>
        internal Statu statu { get; set; }
        internal DateTime? beginDownLoadTime { get; set; }
    }
}
