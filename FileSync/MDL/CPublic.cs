﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSync.MDL
{
    class CPublic
    {
        public static EAF.Log.Log log = new EAF.Log.Log(EAF.Log.LogType.Daily);
        public static DAL.DataContext dc = new DAL.DataContext() { Log = log };
        public static MDL.SyncFileManager syncManager =null;
        
        public static SyncStreamPackage ConvertSyncStreamPackage(byte[] data)
        {
            var ma = UTF8Encoding.UTF8.GetString(data);
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<SyncStreamPackage>(ma);
            }
            catch (Exception ex)
            {
                log.Write( ex.Message+"\r\n"+ma, EAF.Log.MsgType.Error);
                return null;
            }
        }
    }
}
