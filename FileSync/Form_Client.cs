﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSync
{
    public partial class Form_Client : Form
    {
        public Form_Client()
        {
            InitializeComponent();
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void Form_Client_Load(object sender, EventArgs e)
        {
            tClientBindingSource.DataSource = MDL.CPublic.dc.TClient.ToList();
        }

        private void Form_Client_FormClosing(object sender, FormClosingEventArgs e)
        {
            dataGridView1.EndEdit();
            tClientBindingSource.EndEdit();
            var list_tem = tClientBindingSource.DataSource as List<DAL.Tables.TClient>;
            var list_Save = MDL.CPublic.dc.TClient.ToList();
            for (int i = 0; i < list_tem.Count; i++)
                if (string.IsNullOrWhiteSpace(list_tem[i].ClientName))
                    list_tem.RemoveAt(i--);
            foreach (var item in list_Save)
                if (list_tem.Count(c => c.ClientID == item.ClientID) == 0)
                    item.Delete();
            MDL.CPublic.dc.Save(list_Save);
            MDL.CPublic.dc.TClient.Clear();
        }
    }
}
