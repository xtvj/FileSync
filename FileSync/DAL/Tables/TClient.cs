﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDataContext;

namespace FileSync.DAL.Tables
{
    /// <summary>
    /// 客户端列表
    /// </summary>
    [CDataContext.CAttribute.DataTableattirbute(Extended_Property ="客户端列表")]
    public class TClient:CDataContext.CEntity
    {
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AutoIncrement =true,Att_ISPrimary =true)]
        /// <summary></summary>
        public int ClientID
        {
            get;
            set;
        }

        /// <summary>
        /// 连接名
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false,Att_DataLength =50,Extended_Property ="连接名")]
        public string ClientName
        {
            get ;
            set;
        }
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false,Extended_Property ="端口")]
        /// <summary>
        /// 端口
        /// </summary>
        public int ClientPort
        {
            get;
            set;
        }
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false, Att_DataLength =20,Extended_Property ="客户端IP")]
        /// <summary>
        /// 客户端IP
        /// </summary>
        public string ClientIP
        {
            get;
            set;
        }
        public override CEntity[] OnCreated()
        {
            return new TClient[] { new TClient() { ClientIP = "127.0.0.1", ClientPort = 2666, ClientName = "localhost" } };
        }
    }
}
