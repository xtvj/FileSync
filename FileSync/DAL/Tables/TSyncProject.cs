﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSync.DAL.Tables
{
    /// <summary>
    /// 同步项目
    /// </summary>
    [CDataContext.CAttribute.DataTableattirbute(Extended_Property = "同步项目")]
    public class TSyncProject:CDataContext.CEntity
    {
        /// <summary>
        /// 项目ID
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AutoIncrement =true,Att_ISPrimary =true)]
        public int ProjectID
        {
            get;
            set;
        }

        /// <summary>
        /// 项目名称
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false,Att_DataLength =50,Extended_Property = "项目名称")]
        public string ProjectName
        {
            get;
            set;
        }

        /// <summary>
        /// 客户端ID
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false,Extended_Property = "客户端ID")]
        public int ClientID
        {
            get;
            set;
        }

        /// <summary>
        /// 根路径
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false,Extended_Property = "根路径")]
        public string RootPath
        {
            get;
            set;
        }
        /// <summary>
        /// 源文件端
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false,Extended_Property ="源文件端")]
        public bool IsServer { get; set; }
        /// <summary>
        /// 是否充许写入
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false,Extended_Property = "是否充许写入")]
        public bool CanWrite
        {
            get;
            set;
        }

        /// <summary>
        /// 只读Key
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false,Att_DataLength =50,Extended_Property = "只读Key")]
        public string ReadKey
        {
            get;
            set;
        }

        /// <summary>
        /// 写入Key
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_AlowNULL =false,Att_DataLength =50,Extended_Property ="读写Key")]
        public string WriteKey
        {
            get;
            set;
        }
        
        /// <summary>
        /// 扫描时间
        /// </summary>
        [CDataContext.CAttribute.DataColumnAttirbute(Att_IsInDataBase =false)]
        public DateTime ScanTime
        {
            get;
            set;
        }
        [CDataContext.CAttribute.DataColumnAttirbute(Att_IsInDataBase = false)]
        public int CountDir { get; set; }
        [CDataContext.CAttribute.DataColumnAttirbute(Att_IsInDataBase = false)]
        public int CountFile { get; set; }
    }
}
