﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSync.DAL
{
    /// <summary>
    /// 同步监控的目录和文件
    /// </summary>
    public class SyncFile
    {
        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProjectID
        {
            get;
            set;
        }
        /// <summary>
        /// 目录或文件路径
        /// </summary>
        public string FilePath
        {
            get;
            set;
        }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime
        {
            get;
            set;
        }

        /// <summary>
        /// 正在同步
        /// </summary>
        public bool IsSync
        {
            get;
            set;
        }
        /// <summary>
        /// 是否文件
        /// </summary>

        public bool isFile
        {
            get;
            set;
        }
    }
}
