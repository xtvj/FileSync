﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSync.DAL
{
   public  class DataContext:CDataContext.DataContext
    {
        public DataContext() : base( new CDataContext.ConnectionSetting.Conn() { Provider = CDataContext.EProvider.SQLite, DataFile = System.IO.Path.Combine(System.Windows.Forms.Application.StartupPath, "App_Data", "DB.Sqlite") })
        {
            this.CheckAllSchema();
            LoadData();
        }
        public CDataContext.CTable<DAL.Tables.TClient> TClient = new CDataContext.CTable<Tables.TClient>();
        public CDataContext.CTable<DAL.Tables.TSyncProject> TSyncProject = new CDataContext.CTable<Tables.TSyncProject>();
        public void LoadData()
        {
            TClient.Clear();
            TSyncProject.Clear();
            TClient.ToTable();
            TSyncProject.ToTable();
        }
    }
}
