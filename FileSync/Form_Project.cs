﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSync
{
    public partial class Form_Project : Form
    {
        public DAL.Tables.TSyncProject project { get; set; }
        public Form_Project()
        {
            InitializeComponent();
        }

        private void Form_Project_Load(object sender, EventArgs e)
        {
            tClientBindingSource.DataSource = MDL.CPublic.dc.TClient.ToTable().ToArray();
            if (project == null)
                project = new DAL.Tables.TSyncProject() { ScanTime = new DateTime(1900, 1, 1) };
            textBox_ProjectName.Text = string.Format("{0}", project.ProjectName);
            comboBox_Client.SelectedValue = project.ClientID;
            textBox_Path.Text = string.Format("{0}", project.RootPath);
            checkBox_CanWrite.Checked = project.CanWrite;
            textBox_ReadKey.Text = string.Format("{0}", project.ReadKey);
            textBox_WriteKey.Text = string.Format("{0}", project.WriteKey);
            checkBox_IsSource.Checked = project.IsServer;

        }

        private void button_Path_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = textBox_Path.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                textBox_Path.Text = folderBrowserDialog1.SelectedPath;
            var dir = new System.IO.DirectoryInfo(textBox_Path.Text);
            foreach (var file in dir.GetFiles())
            {
                var att = file.Attributes;
            }
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            project.ProjectName = textBox_ProjectName.Text.Trim();
            project.ClientID = ClassLibrary_Public.CConver.getInt(comboBox_Client.SelectedValue);
            project.RootPath = textBox_Path.Text.Trim();
            project.CanWrite = checkBox_CanWrite.Checked;
            project.ReadKey = textBox_ReadKey.Text.Trim();
            project.WriteKey = textBox_WriteKey.Text.Trim();
            project.IsServer = checkBox_IsSource.Checked;
            CheckKey();
            MDL.CPublic.dc.TSyncProject.AddAndSave(project);
            MDL.CPublic.syncManager.UpdateProject(project);
            this.DialogResult = DialogResult.OK;
        }

        private void CheckKey()
        {
            if ((string.IsNullOrWhiteSpace(textBox_ReadKey.Text) || string.IsNullOrWhiteSpace(textBox_WriteKey.Text)) && checkBox_IsSource.Checked)
            {
                do
                {
                    var key_read = Guid.NewGuid().ToString("N").ToUpper();
                    var key_write = Guid.NewGuid().ToString("N").ToUpper();
                    if (MDL.CPublic.dc.TSyncProject.ToTable().Count(c => c.ReadKey == key_read || c.ReadKey == key_write || c.WriteKey == key_read || c.WriteKey == key_write) > 0)
                        continue;
                    textBox_ReadKey.Text = key_read;
                    textBox_WriteKey.Text = key_write;
                    break;
                }
                while (true);
            }
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void checkBox_IsSource_CheckedChanged(object sender, EventArgs e)
        {
            CheckKey();
        }
    }
}
